# arsnova.click V2 Manager

This app manages the file structure of the Project [arsnova.click V2](https://git.thm.de/arsnova/arsnova-click-v2.git). Note that this is not a submodule of the arsnova.click software and is intended to work as a standalone application. The software will clone the required arsnova.click V2 modules and will not modify online data.

It is required to commit and push the changes manually after editing them via the manager.

#### Startup
- Run `npm start` to start the WebApp. Visit `http://localhost:4711`
- Run `npm run start:server` to start the server. The server is using port 3001.
