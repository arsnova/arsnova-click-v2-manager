const corsOptions = require('./cors.config.ts');
const deep = require('deep-diff');
const cors = require('cors');
const fs = require('fs');
const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const http = require('http');
const { spawnSync } = require('child_process');
const app = express();

const cache = { 'arsnova-click-v2-frontend': {}, 'arsnova-click-v2-backend': {} };
const availableLangs = ['en', 'de', 'fr', 'es', 'it'];
const projectGitLocation = {
  'arsnova-click-v2-frontend': path.join(__dirname, '..', 'arsnova-click-v2-frontend'),
  'arsnova-click-v2-backend': path.join(__dirname, '..', 'arsnova-click-v2-backend'),
};
const projectBaseLocation = {
  'arsnova-click-v2-frontend': path.join(__dirname, '..', 'arsnova-click-v2-frontend', 'src'),
  'arsnova-click-v2-backend': path.join(__dirname, '..', 'arsnova-click-v2-backend', 'src'),
};
const projectAppLocation = {
  'arsnova-click-v2-frontend': path.join(__dirname, '..', 'arsnova-click-v2-frontend', 'src', 'app'),
  'arsnova-click-v2-backend': path.join(__dirname, '..', 'arsnova-click-v2-backend', 'src'),
};
const i18nFileBaseLocation = {
  'arsnova-click-v2-frontend': path.join(__dirname, '..', 'arsnova-click-v2-frontend', 'src', 'assets', 'i18n'),
  'arsnova-click-v2-backend': path.join(__dirname, '..', 'arsnova-click-v2-backend', 'assets', 'i18n'),
};

// Parsers
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(cors(corsOptions));
app.param('project', (req, res, next, project) => {
  if (!project || !i18nFileBaseLocation[project]) {
    res.status(500).send({ status: 'STATUS:FAILED', data: 'Invalid Project specified', payload: { project } });
  } else {
    req.i18nFileBaseLocation = i18nFileBaseLocation[project];
    req.projectBaseLocation = projectBaseLocation[project];
    req.projectAppLocation = projectAppLocation[project];
    req.projectGitLocation = projectGitLocation[project];
    req.projectCache = project;
    next();
  }
});
app.options('*', cors(corsOptions));

function deepFind(object, originalPath) {
  originalPath.split('.').forEach((pathElement) => object = object[pathElement]);
  return object;
}

function fromDir(startPath, filter) {
  if (!fs.existsSync(startPath)) {
    console.log('no dir ', startPath);
    return;
  }

  let result = [];

  const files = fs.readdirSync(startPath);
  for (let i = 0; i < files.length; i++) {
    const filename = path.join(startPath, files[i]);
    const stat = fs.lstatSync(filename);
    if (stat.isDirectory()) {
      result = result.concat(fromDir(filename, filter));
    } else if (filter.test(filename)) {
      result.push(filename);
    }
  }
  return result;
}

function objectPath(obj, currentPath = '') {
  let localCurrentPath = currentPath;
  let result = [];

  if (localCurrentPath.length) {
    localCurrentPath = localCurrentPath + '.';
  }
  for (const prop in obj) {
    if (obj.hasOwnProperty(prop)) {
      if (typeof obj[prop] === 'object') {
        result = result.concat(objectPath(obj[prop], localCurrentPath + prop));
      } else {
        result.push(localCurrentPath + prop);
      }
    }
  }
  return result;
}

function isString(data) {
  return typeof data === 'string';
}

function buildKeys({ root, dataNode, langRef, langData }) {

  if (!dataNode) {
    return;
  }

  if (isString(dataNode)) {

    const existingKey = langData.find(elem => elem.key === root);

    if (existingKey) {
      langData.find(elem => elem.key === root).value[langRef] = dataNode;
    } else {
      const value = {};
      value[langRef] = dataNode;
      langData.push({ key: root, value });
    }

  } else {
    Object.keys(dataNode).forEach(key => {
      const rootKey = root ? `${root}.` : '';
      buildKeys({ root: `${rootKey}${key}`, dataNode: dataNode[key], langRef, langData });
    });
  }
}

function createObjectFromKeys({ data, result }) {

  for (const langRef in result) {
    if (result.hasOwnProperty(langRef)) {
      const obj = {};
      data.forEach(elem => {
        const val = elem.value[langRef];
        const objPath = elem.key.split('.');
        objPath.reduce((prevVal, currentVal, index) => {
          if (!prevVal[currentVal]) {
            prevVal[currentVal] = {};
            if (index === objPath.length - 1) {
              prevVal[currentVal] = val;
            }
          }
          return prevVal[currentVal];
        }, obj);

      });
      result[langRef] = { ...result[langRef], ...obj };

    }
  }
}

function getUnusedKeys(req) {
  const result = {};
  const fileNames = fromDir(req.projectAppLocation, /\.(ts|html)$/);
  const langRefs = req.params.langRef ? [req.params.langRef] : availableLangs;

  for (let i = 0; i < langRefs.length; i++) {
    result[langRefs[i]] = [];
    const i18nFileContent = JSON.parse(fs.readFileSync(path.join(req.i18nFileBaseLocation, `${langRefs[i]}.json`)).toString('UTF-8'));
    const objectPaths = objectPath(i18nFileContent);

    objectPaths.forEach((i18nPath => {
      let matched = false;
      fileNames.forEach(filename => {
        if (matched) {
          return;
        }
        const fileContent = fs.readFileSync(filename).toString('UTF-8');
        matched = fileContent.indexOf(i18nPath) > -1;
      });
      if (!matched) {
        result[langRefs[i]].push(i18nPath);
      }
    }));
  }

  return result;
}

function getBranch(req) {
  const command = `git branch 2> /dev/null | sed -e '/^[^*]/d' -e "s/* \\(.*\\)/\\1/"`;
  const child = spawnSync('/bin/sh', [`-c`, command], { cwd: req.projectGitLocation });
  return child.stdout.toString().replace('\n', '');
}

// Angular DIST output folder
app.use(express.static(path.join(__dirname, '..', 'dist')));

app.get('/api/v1/plugin/i18nator/:project/langFile', async (req, res) => {
  const payload = { langData: {}, unused: {}, branch: {} };

  if (!cache[req.projectCache].langData) {
    const langData = [];
    availableLangs.forEach((langRef, index) => {
      buildKeys({
        root: '',
        dataNode: JSON.parse(fs.readFileSync(path.join(req.i18nFileBaseLocation, `${langRef}.json`)).toString('UTF-8')),
        langRef,
        langData,
      });
    });
    cache[req.projectCache].langData = langData;
  }
  payload.langData = cache[req.projectCache].langData;

  if (!cache[req.projectCache].unused) {
    cache[req.projectCache].unused = getUnusedKeys(req);
  }
  payload.unused = cache[req.projectCache].unused;

  if (!cache[req.projectCache].branch) {
    cache[req.projectCache].branch = getBranch(req);
  }
  payload.branch = cache[req.projectCache].branch;

  res.send({ status: 'STATUS:SUCCESSFUL', payload });
});

app.post('/api/v1/plugin/i18nator/:project/updateLang', async (req, res) => {
  if (!req.body.data) {
    res.status(500).send({ status: 'STATUS:FAILED', data: 'Invalid Data', payload: { body: req.body } });
    return;
  }

  const result = { en: {}, de: {}, es: {}, fr: {}, it: {} };
  const langKeys = Object.keys(result);
  createObjectFromKeys({ data: req.body.data, result });

  cache[req.projectCache].langData = req.body.data;

  langKeys.forEach((langRef, index) => {
    const fileContent = result[langRef];
    const fileLocation = path.join(req.i18nFileBaseLocation, `${langRef}.json`);
    const exists = fs.existsSync(fileLocation);
    if (!exists) {
      res.status(404).send({ status: 'STATUS:FAILED', data: 'File not found', payload: { fileLocation } });
      return;
    }
    fs.writeFileSync(fileLocation, JSON.stringify(fileContent));
    if (index === langKeys.length - 1) {
      res.send({ status: 'STATUS:SUCCESSFUL' });
    }
  });
});

// Send all other requests to the Angular app
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, '..', 'dist', 'index.html'));
});

// Set Port
const port = process.env.PORT || '3001';
app.set('port', port);

const server = http.createServer(app);

server.listen(port, () => {
  console.log(`Running on localhost:${port}`);

  Object.keys(cache).forEach(projectName => {
    console.log(``);
    console.log(`------- Building cache for '${projectName}' -------`);

    console.log(`* Fetching language data`);
    const langDataStart = new Date().getTime();
    const langData = [];
    availableLangs.forEach((langRef, index) => {
      buildKeys({
        root: '',
        dataNode: JSON.parse(fs.readFileSync(path.join(i18nFileBaseLocation[projectName], `${langRef}.json`)).toString('UTF-8')),
        langRef,
        langData,
      });
    });
    cache[projectName].langData = langData;
    const langDataEnd = new Date().getTime();
    console.log(`-- Done. Took ${langDataEnd - langDataStart}ms`);

    console.log(`* Fetching unused keys`);
    const unusedKeysStart = new Date().getTime();
    cache[projectName].unused = getUnusedKeys({
      params: {},
      projectAppLocation: projectAppLocation[projectName],
      i18nFileBaseLocation: i18nFileBaseLocation[projectName],
    });
    const unusedKeysEnd = new Date().getTime();
    console.log(`-- Done. Took ${unusedKeysEnd - unusedKeysStart}ms`);

    console.log(`* Fetching active git branch`);
    const gitBranchStart = new Date().getTime();
    cache[projectName].branch = getBranch({
      projectGitLocation: projectGitLocation[projectName],
    });
    const gitBranchEnd = new Date().getTime();
    console.log(`-- Done. Took ${gitBranchEnd - gitBranchStart}ms`);

  });
  console.log(``);
  console.log(`Cache built successfully`);
});
