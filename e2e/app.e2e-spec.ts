import { ArsnovaClickV2ManagerPage } from './app.po';

describe('arsnova-click-v2-manager App', () => {
  let page: ArsnovaClickV2ManagerPage;

  beforeEach(() => {
    page = new ArsnovaClickV2ManagerPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
