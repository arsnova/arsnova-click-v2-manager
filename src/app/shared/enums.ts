export enum FILTER {
  NONE, UNUSED, INVALID_KEYS, INVALID_DE, INVALID_EN, INVALID_ES, INVALID_FR, INVALID_IT
}

export enum LANGUAGE {
  en = 'en', de = 'de', fr = 'fr', es = 'es', it = 'it'
}

export enum PROJECT {
  FRONTEND = 'arsnova-click-v2-frontend',
  BACKEND = 'arsnova-click-v2-backend',
}
