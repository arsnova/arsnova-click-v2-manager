export interface IDiffData {
  kind: 'D' | 'A' | 'N',
  'E',
  path: Array<string>,
  lhs?: string,
  rhs?: string
}
