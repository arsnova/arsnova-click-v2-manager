import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {ModalOrganizerService} from './modal-organizer.service';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    BrowserModule,
    HttpClientModule,
    RouterModule,
  ],
  declarations: [],
  exports: [
    CommonModule,
    NgbModule,
    BrowserModule,
    HttpClientModule,
    RouterModule
  ],
  providers: [
    ModalOrganizerService
  ]
})
export class SharedModule { }
