import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { LANGUAGE } from '../shared/enums';
import { ProjectLoaderService } from './project-loader.service';

@Injectable({
  providedIn: 'root',
})
export class LanguageLoaderService {
  public readonly languages = LANGUAGE;

  private _apiRef;

  get apiRef(): string {
    return `${this.projectLoaderService.apiRef}/langFile`;
  }

  private _updateProjectApi;

  get updateProjectApi() {
    return `${this.projectLoaderService.apiRef}`;
  }

  private _unusedKeysApi;

  get unusedKeysApi() {
    return `${this.projectLoaderService.apiRef}/unusedKeys`;
  }

  private _parsedLangData = [];

  get parsedLangData(): Array<any> {
    return this._parsedLangData;
  }

  private _unusedKeys = {};

  get unusedKeys(): {} {
    return this._unusedKeys;
  }

  constructor(
    private http: HttpClient,
    private projectLoaderService: ProjectLoaderService,
  ) {
  }

  public reset(): void {
    this._parsedLangData = [];
  }

  public getLangData(): void {
    this.http.get(`${this.apiRef}`).subscribe((response: any) => {
      if (response.status !== 'STATUS:SUCCESSFUL') {
        this.projectLoaderService.connected = false;
        return;
      }
      this.projectLoaderService.connected = true;
      this._parsedLangData = response.payload.langData;
      this._unusedKeys = response.payload.unused;
      this.projectLoaderService.currentBranch = response.payload.branch;
    });
  }

  updateProject() {
    this.http.post(`${this.updateProjectApi}/updateLang`, {
      data: this.parsedLangData,
    }).subscribe((response: any) => {
      if (response.status !== 'STATUS:SUCCESSFUL') {
        console.log(response);
      }
    });
  }
}
