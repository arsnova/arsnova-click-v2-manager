import { Injectable } from '@angular/core';
import { PROJECT } from '../shared/enums';

@Injectable({
  providedIn: 'root',
})
export class ProjectLoaderService {
  public readonly projects = PROJECT;

  private _apiRef = `http://${location.hostname}:3001/api/v1/plugin/i18nator`;

  get apiRef(): string {
    return `${this._apiRef}/${this._currentProject}`;
  }

  private _connected = false;

  get connected(): boolean {
    return this._connected;
  }

  set connected(value: boolean) {
    this._connected = value;
  }

  private _currentProject = PROJECT.FRONTEND;

  get currentProject(): PROJECT {
    return this._currentProject;
  }

  set currentProject(value: PROJECT) {
    this._currentProject = value;
    this._connected = false;
  }

  private _currentBranch = '';

  get currentBranch(): string {
    return this._currentBranch;
  }

  set currentBranch(value: string) {
    this._currentBranch = value;
  }

  constructor() { }
}
