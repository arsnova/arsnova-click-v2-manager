import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FilterKeysPipe } from './filter-keys.pipe';
import { JustAFewPipe } from './justafew.pipe';
import { SearchFilterPipe } from './search-filter.pipe';
import { SortPipe } from './sort.pipe';

@NgModule({
  imports: [CommonModule],
  exports: [JustAFewPipe, FilterKeysPipe, SearchFilterPipe, SortPipe],
  declarations: [JustAFewPipe, FilterKeysPipe, SearchFilterPipe, SortPipe],
})
export class PipesModule {
  static forRoot() {
    return {
      ngModule: PipesModule,
      providers: [],
    };
  }
}
