import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { ModalsModule } from './modals/modals.module';
import { NavigationComponent } from './navigation/navigation.component';
import { PipesModule } from './pipes/pipes.module';
import { I18nManagerComponent } from './plugin/i18n-manager/i18n-manager.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'plugin/i18nManager',
    component: I18nManagerComponent,
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HomeComponent,
  ],
  imports: [
    CommonModule,
    PipesModule.forRoot(),
    RouterModule.forRoot(routes),
    NgbModule.forRoot(),
    ModalsModule,
  ],
  providers: [],
  exports: [RouterModule],
  bootstrap: [AppComponent],
})
export class AppModule {
}
