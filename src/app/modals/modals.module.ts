import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { AddModeComponent } from './add-mode/add-mode.component';

@NgModule({
  imports: [
    SharedModule,
  ],
  declarations: [AddModeComponent],
  entryComponents: [AddModeComponent],
})
export class ModalsModule {
}
