import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LanguageLoaderService } from '../../services/language-loader.service';

@Component({
  selector: 'app-add-mode',
  templateUrl: './add-mode.component.html',
  styleUrls: ['./add-mode.component.scss'],
})
export class AddModeComponent {

  public dataMap;
  public key = '';
  public value = {};
  private scrollY = window.scrollY;

  constructor(
    private activeModal: NgbActiveModal,
    public languageLoaderService: LanguageLoaderService,
  ) {
  }

  public dismiss(result?): void {
    window.scroll(0, this.scrollY);
    this.activeModal.dismiss(result);
  }

  public updateKey(event, langRef): void {
    this.value[langRef] = event.target.value;
  }

  public getKeys(dataNode: object): Array<string> {
    if (!dataNode) {
      return [];
    }
    return Object.keys(dataNode).sort();
  }

  public save() {
    if (!this.key.length) {
      return;
    }

    this.dataMap.push({ key: this.key, value: this.value });

    window.scroll(0, this.scrollY);
    this.activeModal.close({ dataMap: this.dataMap });
  }
}
