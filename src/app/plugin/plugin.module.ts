import { NgModule } from '@angular/core';
import { PipesModule } from '../pipes/pipes.module';
import { SharedModule } from '../shared/shared.module';
import { I18nManagerModule } from './i18n-manager/i18n-manager.module';

@NgModule({
  imports: [
    SharedModule,
    PipesModule,
    I18nManagerModule,
  ],
  providers: [],
  declarations: [],
})
export class PluginModule {
}
