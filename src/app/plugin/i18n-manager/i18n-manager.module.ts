import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { PipesModule } from '../../pipes/pipes.module';
import { SharedModule } from '../../shared/shared.module';
import { I18nManagerComponent } from './i18n-manager.component';
import { KeyOutputComponent } from './key-output/key-output.component';

@NgModule({
  imports: [
    SharedModule,
    PipesModule,
    RouterModule,
  ],
  declarations: [
    I18nManagerComponent,
    KeyOutputComponent,
  ],
})
export class I18nManagerModule {
}
