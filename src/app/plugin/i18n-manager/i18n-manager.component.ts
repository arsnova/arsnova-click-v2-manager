import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { of } from 'rxjs/index';
import { Observable } from 'rxjs/Rx';
import { LanguageLoaderService } from '../../services/language-loader.service';
import { ProjectLoaderService } from '../../services/project-loader.service';
import { FILTER, PROJECT } from '../../shared/enums';
import { ModalOrganizerService } from '../../shared/modal-organizer.service';

@Component({
  selector: 'app-i18n-manager',
  templateUrl: './i18n-manager.component.html',
  styleUrls: ['./i18n-manager.component.scss'],
})
export class I18nManagerComponent implements OnInit {
  public readonly filters = FILTER;

  private _langRef = ['en', 'de', 'fr', 'it', 'es'];

  get langRef(): Array<string> {
    return this._langRef;
  }

  private _selectedKey: { key: string, value: string };

  get selectedKey(): { key: string; value: string } {
    return this._selectedKey;
  }

  private _changedData = false;

  get changedData(): boolean {
    return this._changedData;
  }

  private _searchFilter = '';

  get searchFilter(): string {
    return this._searchFilter;
  }

  set searchFilter(value: string) {
    this._searchFilter = value;
  }

  private _filter = FILTER.NONE;

  get filter(): FILTER {
    return this._filter;
  }

  set filter(value: FILTER) {
    this.hasAnyMatches = of(false);
    switch (parseInt(String(value), 10)) {
      case 0:
        this._filter = FILTER.NONE;
        return;
      case 1:
        this._filter = FILTER.UNUSED;
        return;
      case 2:
        this._filter = FILTER.INVALID_KEYS;
        return;
      case 3:
        this._filter = FILTER.INVALID_DE;
        return;
      case 4:
        this._filter = FILTER.INVALID_EN;
        return;
      case 5:
        this._filter = FILTER.INVALID_ES;
        return;
      case 6:
        this._filter = FILTER.INVALID_FR;
        return;
      case 7:
        this._filter = FILTER.INVALID_IT;
        return;
      default:
        throw Error(`Unknown filter set: ${value}`);
    }
  }

  private _hasAnyMatches = of(false);

  get hasAnyMatches(): Observable<boolean> {
    return this._hasAnyMatches;
  }

  set hasAnyMatches(value: Observable<boolean>) {
    this._hasAnyMatches = value;
  }

  constructor(
    private http: HttpClient,
    public modalOrganizerService: ModalOrganizerService,
    public projectLoaderService: ProjectLoaderService,
    private languageLoaderService: LanguageLoaderService,
  ) {
  }

  ngOnInit() {
    this.setProject(PROJECT.FRONTEND);
  }

  public updateData(): void {
    this.languageLoaderService.updateProject();
    this._changedData = false;
  }

  public changeFilter(filter: number) {
    this.filter = filter;
    this._selectedKey = null;
  }

  public setProject(value: PROJECT): void {
    this._selectedKey = undefined;
    this.languageLoaderService.reset();
    this.projectLoaderService.currentProject = value;
    this.reloadLanguageData();
  }

  public dataChanged(key): void {
    this._selectedKey = key;
  }

  public getKeys(dataNode: Array<string>): Array<string> {
    if (!dataNode) {
      return [];
    }
    return Object.keys(dataNode).sort();
  }

  public updateKey(event, langRef, key) {
    const value = event.target.value;

    this._changedData = true;

    if (!value.length) {
      delete key.value[langRef];

    } else {
      key.value[langRef] = value;

    }

  }

  private reloadLanguageData(): void {
    this.languageLoaderService.getLangData();
  }

}
